<?php

namespace App\Http\Controllers;

use App\ServiceCategory;
use Illuminate\Http\Request;

class ServiceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['main'] = 'Service Category';
        $data['active'] = 'View Service Category';
        $data['categorys'] = ServiceCategory::all();

        return view('servicesCategory.servicesCategory_index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['main'] = 'Service Category';
        $data['active'] = 'Add Service Category';
        $data['title'] = 'Service Category registration form';
        return view('servicesCategory.servicesCategory_create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'service_category_name' => 'required',


        ]);


        $result = ServiceCategory::create($request->all());
        if ($result) {
            return redirect()->route('serviceCatogory.index')
                ->with('success', 'created successfully.');
        } else {
            return redirect()->route('serviceCatogory.create')
                ->with('error', 'Some things to insert data');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceCategory $serviceCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceCategory $serviceCategory)
    {
        $data['main'] = 'Positions';
        $data['active'] = 'Update Position';
        $data['title'] = 'Position update registration form';

        echo '<pre>';
        print_r($serviceCategory);exit();

        #return view('positions.positions_edit", $data);
        return view('servicesCategory.servicesCategory_edit', $data, compact('serviceCategory'));
    }


    public function update(Request $request, ServiceCategory $serviceCategory)
    {
        $request->validate([

            'service_category_name' => 'required',


        ]);


        $serviceCategory->update($request->all());


        return redirect()->route('serviceCatogory.index')
            ->with('success', 'updated successfully');
    }


    public function destroy(ServiceCategory $service)
    {
        $id=$service->delete();
        return redirect()->route('serviceCatogory.index')
            ->with('success','deleted successfully');
    }
}
