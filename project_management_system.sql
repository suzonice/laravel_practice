-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2019 at 09:32 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_management_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `area_id` bigint(20) UNSIGNED NOT NULL,
  `devision_id` int(11) NOT NULL,
  `area_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`area_id`, `devision_id`, `area_name`, `created_at`, `updated_at`) VALUES
(8, 3, 'Mirpur', '2019-06-22 06:30:26', '2019-06-22 06:30:26'),
(9, 1, 'Mirpur', '2019-06-22 07:49:42', '2019-06-22 07:49:42'),
(10, 1, 'Mohamadpur', '2019-06-22 07:50:37', '2019-06-22 07:50:37'),
(11, 1, 'Dhanmondi', '2019-06-22 07:53:29', '2019-06-22 07:53:29'),
(12, 1, 'Gulshan', '2019-06-22 07:53:45', '2019-06-22 07:53:45'),
(13, 1, 'Badda', '2019-06-22 07:53:59', '2019-06-22 07:53:59'),
(14, 1, 'Banani', '2019-06-22 07:54:18', '2019-06-22 07:54:18'),
(15, 1, 'Baridhara', '2019-06-22 07:54:35', '2019-06-22 07:54:35'),
(16, 1, 'Basundhara', '2019-06-22 07:54:50', '2019-06-22 07:54:50'),
(17, 1, 'Cantonment', '2019-06-22 07:55:04', '2019-06-22 07:55:04'),
(18, 1, 'Elephant Road', '2019-06-22 07:55:27', '2019-06-22 07:55:27'),
(19, 1, 'Jatrabari', '2019-06-22 07:55:40', '2019-06-22 07:55:40'),
(20, 1, 'Lalbag', '2019-06-22 07:56:00', '2019-06-22 07:56:00'),
(21, 1, 'Malibag-Mogbazar', '2019-06-22 07:56:11', '2019-06-22 07:58:41'),
(22, 1, 'Mohakhali', '2019-06-22 07:56:28', '2019-06-22 07:56:28'),
(23, 1, 'Motijheel', '2019-06-22 07:56:41', '2019-06-22 07:56:41');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_mobile_two` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `division_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `profession_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `customer_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_remendar_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_name`, `customer_mobile`, `customer_mobile_two`, `customer_email`, `division_id`, `area_id`, `position_id`, `profession_id`, `type_id`, `customer_remark`, `customer_address`, `customer_remendar_date`, `created_at`, `updated_at`) VALUES
(17, 'Siddikur', '01738305670', NULL, 'suzongmai@gmail.com', 1, 9, 12, 13, 5, 'ggg', 'gg', '0000-00-00', '2019-06-22 08:07:40', '2019-06-22 08:17:12'),
(19, 'Siddikur', '01738305670', '01738305670', 'suzonfgmai@gmail.com', 3, 8, 7, 2, 3, NULL, NULL, '0000-00-00', '2019-06-22 23:44:38', '2019-06-22 23:44:38'),
(20, NULL, NULL, NULL, NULL, 1, 10, 7, 2, 3, NULL, NULL, '2019-06-20', '2019-06-23 00:29:57', '2019-06-23 00:29:57'),
(21, 'Siddikur rohman', '0173830564', '017383026470', 'suzong1mai@gmail.com', 1, 9, 7, 2, 3, NULL, NULL, '2019-06-23', '2019-06-23 00:32:00', '2019-06-23 00:32:00'),
(22, 'Siddikur', '0173830564', '0173830564701', NULL, 1, 12, 7, 2, 3, NULL, NULL, '2019-06-25', '2019-06-23 00:38:43', '2019-06-23 00:53:10');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `division_id` int(11) UNSIGNED NOT NULL,
  `division_name` varchar(30) NOT NULL,
  `bn_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`division_id`, `division_name`, `bn_name`) VALUES
(1, 'Dhaka', 'ঢাকা'),
(2, 'Chattogram', 'চট্টগ্রাম'),
(3, 'Barishal', 'বরিশাল'),
(4, 'Khulna', 'খুলনা'),
(5, 'Rajshahi', 'রাজশাহী'),
(6, 'Rangpur', 'রংপুর'),
(7, 'Sylhet', 'সিলেট'),
(8, 'Mymensingh', 'ময়মনসিংহ'),
(9, 'Unknown', '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_06_18_083021_create_customers_table', 1),
(4, '2019_06_18_083944_create_areas_table', 2),
(5, '2019_06_18_084034_create_professions_table', 2),
(6, '2019_06_18_084109_create_positions_table', 2),
(7, '2019_06_18_084126_create_types_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `position_id` bigint(20) UNSIGNED NOT NULL,
  `position_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`position_id`, `position_name`, `created_at`, `updated_at`) VALUES
(7, 'MD', '2019-06-18 06:48:42', '2019-06-22 08:03:47'),
(8, 'Manager', '2019-06-18 07:56:28', '2019-06-18 07:56:28'),
(12, 'CEO', '2019-06-22 08:04:02', '2019-06-22 08:04:02'),
(13, 'Asst. Manager', '2019-06-22 08:04:31', '2019-06-22 08:04:31');

-- --------------------------------------------------------

--
-- Table structure for table `professions`
--

CREATE TABLE `professions` (
  `profession_id` bigint(20) UNSIGNED NOT NULL,
  `profession_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `professions`
--

INSERT INTO `professions` (`profession_id`, `profession_name`, `created_at`, `updated_at`) VALUES
(2, 'doctor', '2019-06-18 06:11:21', '2019-06-18 06:11:21'),
(4, 'engineering', '2019-06-18 06:31:11', '2019-06-18 06:31:11'),
(5, 'professor', '2019-06-18 06:31:49', '2019-06-18 06:31:49'),
(7, 'teacher', '2019-06-18 07:56:49', '2019-06-18 07:56:49'),
(8, 'Business', '2019-06-22 06:49:15', '2019-06-22 06:49:15'),
(9, 'Gov. Job', '2019-06-22 06:50:01', '2019-06-22 06:50:01'),
(10, 'Private Job', '2019-06-22 06:50:20', '2019-06-22 06:50:20'),
(11, 'Banker', '2019-06-22 06:52:09', '2019-06-22 06:52:09'),
(12, 'Journalists', '2019-06-22 06:56:12', '2019-06-22 06:56:12'),
(13, 'Lawyer', '2019-06-22 06:56:28', '2019-06-22 23:27:47'),
(14, 'Army / Military', '2019-06-22 06:56:57', '2019-06-22 06:56:57'),
(15, 'Models', '2019-06-22 06:57:07', '2019-06-22 06:57:07'),
(16, 'Painters', '2019-06-22 06:57:32', '2019-06-22 06:57:32'),
(17, 'Photographers', '2019-06-22 06:57:46', '2019-06-22 06:57:46'),
(18, 'Police', '2019-06-22 06:58:09', '2019-06-22 06:58:09'),
(19, 'Programmers', '2019-06-22 06:58:22', '2019-06-22 06:58:22'),
(20, 'Scientists', '2019-06-22 06:58:35', '2019-06-22 06:58:35'),
(21, 'Social workers', '2019-06-22 06:58:47', '2019-06-22 06:58:47'),
(22, 'Students', '2019-06-22 06:59:07', '2019-06-22 06:59:07'),
(23, 'Tailors', '2019-06-22 06:59:17', '2019-06-22 06:59:17'),
(24, 'Tanners', '2019-06-22 06:59:28', '2019-06-22 06:59:28'),
(25, 'Designer', '2019-06-22 07:00:00', '2019-06-22 07:00:00'),
(26, 'Driver', '2019-06-22 07:00:13', '2019-06-22 07:00:13');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`type_id`, `type_name`, `created_at`, `updated_at`) VALUES
(3, 'IT', '2019-06-18 07:57:07', '2019-06-22 08:05:18'),
(4, 'Medical', NULL, '2019-06-22 08:05:31'),
(5, 'Buseness', '2019-06-22 06:41:06', '2019-06-22 06:41:06'),
(6, 'It Buseness', '2019-06-22 06:41:18', '2019-06-22 06:41:18'),
(7, 'Fashaion', '2019-06-22 06:41:32', '2019-06-22 06:41:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shahinul isam', 'suzonice15@gmail.com', NULL, '9e07a605cd000f078a3fa3ac11b53761', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`area_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`division_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `professions`
--
ALTER TABLE `professions`
  ADD PRIMARY KEY (`profession_id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `area_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `position_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `professions`
--
ALTER TABLE `professions`
  MODIFY `profession_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `type_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
